from django import forms
from .models import Transfer

class TransferForm(forms.Form):
    nama_form = forms.CharField(label='Nama Pengirim', max_length=50, 
                                widget=forms.TextInput(attrs={'class': 'form-control'}))
    rekening_form = forms.IntegerField(label='Nomor Rekening',
                                widget=forms.TextInput(attrs={'class': 'form-control'}))
    nominal_form = forms.IntegerField(label='Nominal',
                                widget=forms.TextInput(attrs={'class': 'form-control'}))
    berita_form = forms.CharField(label ='Berita', widget=forms.Textarea)