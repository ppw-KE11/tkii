from django.shortcuts import render, redirect   
from .models import Transfer
from .forms import TransferForm

# Create your views here.
def transfer(request):
    form_transfer = TransferForm(request.POST or None)
    if request.method == 'POST':
        if form_transfer.is_valid(): 
            Transfer.objects.create(
                nama = request.POST['nama_form'],
                rekening = request.POST['rekening_form'],
                nominal = request.POST['nominal_form'],
                berita = request.POST['berita_form']
            ) 
            return redirect('status')
    context = {
        'form': form_transfer
    }
    return render(request, 'transfer.html', context)

def status(request):
    obj = Transfer.objects.all().order_by("-waktu")
    context = {
        'data' : obj
    }
    return render(request, 'status.html', context)