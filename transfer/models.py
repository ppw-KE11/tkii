from django.db import models
from django.utils import timezone

# Create your models here.
class Transfer(models.Model):
    nama = models.CharField(max_length=50)
    rekening = models.IntegerField()
    nominal = models.IntegerField()
    berita = models.CharField(max_length=100)
    waktu = models.DateTimeField(default=timezone.now)