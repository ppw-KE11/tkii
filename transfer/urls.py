from django.urls import path, include
from .views import transfer, status

urlpatterns = [
    path('form/', transfer, name='transfer'),
    path('status/', status, name='status')
]