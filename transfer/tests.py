from django.test import TestCase
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import transfer
from .models import Transfer
from .forms import TransferForm
from django.utils import timezone

# Create your tests here.

class TransferTest(TestCase):
    def test_transfer_url_is_exist(self):
        response = Client().get('/transfer/form')
        self.assertEqual(response.status_code, 301)

    def test_status_url_is_exist(self):
        response = Client().get('/transfer/status')
        self.assertEqual(response.status_code, 301)

    def do_transfer(self,
            nama = 'Hamam',
            rekening = '1234567890',
            nominal = '1000000',
            berita = 'tes transfer',
            waktu = timezone.now()
        ):
        return Transfer.objects.create(
            nama = nama,
            rekening = rekening,
            nominal = nominal,
            berita = berita,
            waktu = timezone.now()
    )
    
    def test_do_transfer(self):
        obj = self.do_transfer()
        self.assertTrue(isinstance(obj, Transfer))

    def test_valid_form(self):
        obj = Transfer.objects.create(
            nama = 'Hamam',
            rekening = '1234567890',
            nominal = '1000000',
            berita = 'tes form transfer',
        )

        data = {
            'nama': obj.nama,
            'rekening': obj.rekening,
            'nominal': obj.nominal,
            'berita': obj.berita
        }

        form = TransferForm(data=data)
        self.assertTrue(form)

    def test_invalid_form(self):
        obj = Transfer.objects.create(
            nama = '',
            rekening = '1234567890',
            nominal = '1000000',
            berita = 'tes form transfer',
        )

        data = {
            'nama': obj.nama,
            'rekening': obj.rekening,
            'nominal': obj.nominal,
            'berita': obj.berita
        }

        form = TransferForm(data=data)
        self.assertFalse(form.is_valid())

    

    