from django.db import models

# Create your models here.
class BayarListrik(models.Model):
    nama = models.CharField(max_length=30)
    token = models.CharField(max_length=9)
    tarif = models.CharField(max_length=9)