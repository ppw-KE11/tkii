from django.shortcuts import render,redirect
from .forms import FormBayarListrik
from .models import BayarListrik
from django.http import HttpResponse
from django.http import HttpResponseRedirect

response = {}
# Create your views here.
def epay(request):
    return render(request, 'epay.html', {'form': FormBayarListrik})

def epaycontents(request):
    contents = FormBayarListrik.objects.all().values()
    response['contents'] = convert_quaryset_into_json(contents)
    response['form'] = FormBayarListrik
    return render(request, 'epay.html', response)

def send(request):
    form = FormBayarListrik(request.POST)

    if request.method == 'POST':
        form = FormBayarListrik(request.POST)
        if form.is_valid():
            log = BayarListrik(nama = request.POST['nama'], token = request.POST['token'], tarif = request.POST['tarif'])
            log.save()
        return redirect("/success/")
    else:
        form = FormBayarListrik

def convert_quaryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val

def epaysuccess(request):
    return render (request, 'epaysuccess.html')