from django.apps import AppConfig


class Kel11AppConfig(AppConfig):
    name = 'kel11app'
