from django.test import TestCase, Client
from .views import epay

# Create your tests here.

class GroupProjectUnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_doesnt_exist(self):
        response = Client().get('/notfound/')
        self.assertEqual(response.status_code, 404)

    def test_url_using_epay_page(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'epay.html')
