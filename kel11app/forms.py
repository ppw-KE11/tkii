from django import forms

class FormBayarListrik(forms.Form):
    attribute = {
        'class' : 'form-control'
    }

    nama = forms.CharField(max_length=30, label='Nama', required=True, widget=forms.TextInput(attrs=attribute))
    token = forms.CharField(max_length=9, label='Nomor Token', required=True, widget=forms.TextInput(attrs=attribute))
    tarif = forms.CharField(max_length=9, label='Tarif', required=True, widget=forms.TextInput(attrs=attribute))