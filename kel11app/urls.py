from django.contrib import admin
from django.urls import path
from . import views

app_name = 'kel11app'

urlpatterns = [
    path('', views.epay, name='epay'),
    path('success/', views.epaysuccess, name='epaysuccess'),
]